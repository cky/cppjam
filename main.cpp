#include "c++jam.h"

#include <functional>
#include <iostream>
#include <numeric>
#include <boost/format.hpp>
#include "ops.h"
#include "utils.h"

namespace {
    struct lambda_stack_op : cppjam::stack_op_t {
        std::function<cppjam::stack_t(cppjam::stack_t const&)> func_;

        explicit lambda_stack_op(std::function<cppjam::stack_t(cppjam::stack_t const&)> func)
                : func_(std::move(func)) {}

        char const* repr() const override {return nullptr;}

        cppjam::stack_t operator()(cppjam::stack_t const& st) const override {
            return func_(st);
        }
    };

    cppjam::stack_t stack(std::initializer_list<cppjam::datum> items) {
        return std::accumulate(std::begin(items), std::end(items), cppjam::stack_t(),
                [](cppjam::stack_t const& st, cppjam::datum const& item) {
            return cppjam::stack_t(item, st);
        });
    }

    cppjam::array_t int_array(std::initializer_list<cppjam::int_t> ints) {
        return cppjam::array_t(std::begin(ints), std::end(ints));
    }

    std::shared_ptr<cppjam::stack_op_t> lambda(std::function<cppjam::stack_t(cppjam::stack_t const&)> func) {
        return std::make_shared<lambda_stack_op>(std::move(func));
    }

    std::shared_ptr<cppjam::stack_op_t> lambda1(std::function<cppjam::datum(cppjam::datum const&)> func) {
        return lambda([=](cppjam::stack_t const& st) {
            return cppjam::stack_t(func(st.car()), st.cdr());
        });
    }

    // Later, when there's a real parser, the code could evaluate
    // `expr` and compare its result to `value`. But for now, just
    // print `value` and manually verify the results.
    void run(char const* expr, cppjam::stack_t const& value) {
        std::cout << boost::format("%1% => %2%\n") % expr % cppjam::utils::repr(value);
    }
}

int main() try {
    using namespace cppjam;
    using cppjam::float_t;
    using cppjam::stack_t;

    run("0!", lookup_op("!")(stack({int_t(0)})));
    run("13.25!", lookup_op("!")(stack({float_t(13.25)})));
    run("[]!", lookup_op("!")(stack({array_t()})));
    run(R"("foo"!)", lookup_op("!")(stack({utils::string_to_datum(U"foo")})));

    run("2 4#", lookup_op("#")(stack({int_t(2), int_t(4)})));
    run("10-2#", lookup_op("#")(stack({int_t(10), int_t(-2)})));
    run("6.25 0.5#", lookup_op("#")(stack({float_t(6.25), float_t(0.5)})));
    run("[1 3 5]5#", lookup_op("#")(stack({int_array({1, 3, 5}), int_t(5)})));
    run("[1 3 5]2#", lookup_op("#")(stack({int_array({1, 3, 5}), int_t(2)})));
    run(R"('o"foo"#)", lookup_op("#")(stack({utils::char_to_datum(U'o'), utils::string_to_datum(U"foo")})));
    run(R"("abcde""cd"#)", lookup_op("#")(stack({utils::string_to_datum(U"abcde"), utils::string_to_datum(U"cd")})));
    run("[1 3 5][1 5]#", lookup_op("#")(stack({int_array({1, 3, 5}), int_array({1, 5})})));
    run(R"(["ab""cd"]["ab"]#)", lookup_op("#")(stack({array_t{utils::string_to_datum(U"ab"), utils::string_to_datum(U"cd")}, array_t{utils::string_to_datum(U"ab")}})));
    run("[1 7 5]{4>}#", lookup_op("#")(stack({int_array({1, 7, 5}), lambda1([](datum const& value) {return utils::bool_to_datum(boost::get<int_t>(value) > 4);})})));
    run(R"({'A<}"foo"#)", lookup_op("#")(stack({lambda1([](datum const& value) {return utils::bool_to_datum(boost::get<char_t>(value) < U'A');}), utils::string_to_datum(U"foo")})));

    run("3 5 1$", lookup_op("$")(stack({int_t(3), int_t(5), int_t(1)})));
    run("3 5 -1.5$", lookup_op("$")(stack({int_t(3), int_t(5), float_t(-1.5)})));
    run(R"("hello"$)", lookup_op("$")(stack({utils::string_to_datum(U"hello")})));
    run("[3 1.5 2]$", lookup_op("$")(stack({array_t{int_t(3), float_t(1.5), int_t(2)}})));
    run(R"(["foo""bar"]$)", lookup_op("$")(stack({array_t{utils::string_to_datum(U"foo"), utils::string_to_datum(U"bar")}})));
    run("[1 4 3 5 2]{W*}$", lookup_op("$")(stack({int_array({1, 4, 3, 5, 2}), lambda1([](datum const& value) {return -boost::get<int_t>(value);})})));
    run("[2 -3 4 -5]{0>}$", lookup_op("$")(stack({int_array({2, -3, 4, -5}), lambda1([](datum const& value) {return utils::bool_to_datum(boost::get<int_t>(value) > 0);})})));

    run("4,", lookup_op(",")(stack({int_t(4)})));
    run("'f,'c,-", lookup_op("-")(lookup_op(",")(stack_t(utils::char_to_datum(U'c'), lookup_op(",")(stack({utils::char_to_datum(U'f')}))))));
    run(R"("hello",)", lookup_op(",")(stack({utils::string_to_datum(U"hello")})));
    run("10,,", lookup_op(",")(lookup_op(",")(stack({int_t(10)}))));
    run("[2 -3 1]{0>},", lookup_op(",")(stack({int_array({2, -3, 1}), lambda1([](datum const& value) {return utils::bool_to_datum(boost::get<int_t>(value) > 0);})})));
    run("10{3%},", lookup_op(",")(stack({int_t(10), lambda1([](datum const& value) {return int_t(boost::get<int_t>(value) % 3);})})));

    run("5 3-", lookup_op("-")(stack({int_t(5), int_t(3)})));
    run("'D'A-", lookup_op("-")(stack({utils::char_to_datum(U'D'), utils::char_to_datum(U'A')})));
    run("'D3-", lookup_op("-")(stack({utils::char_to_datum(U'D'), int_t(3)})));
    run("[1 2 3 2 1]2-", lookup_op("-")(stack({int_array({1, 2, 3, 2, 1}), int_t(2)})));
    run(R"("hello""hl"-)", lookup_op("-")(stack({utils::string_to_datum(U"hello"), utils::string_to_datum(U"hl")})));
    run("2[1 3]-", lookup_op("-")(stack({int_t(2), int_array({1, 3})})));

    run("2 3;", lookup_op(";")(stack({int_t(2), int_t(3)})));

    run(R"(12"foo""bar"@)", lookup_op("@")(stack({int_t(12), utils::string_to_datum(U"foo"), utils::string_to_datum(U"bar")})));

    run(R"(5"hi"\)", lookup_op("\\")(stack({int_t(5), utils::string_to_datum(U"hi")})));

    run(R"("hi"_)", lookup_op("_")(stack({utils::string_to_datum(U"hi")})));

    run("'Aa", lookup_op("a")(stack({utils::char_to_datum(U'A')})));
    run("[1 2]a", lookup_op("a")(stack({int_array({1, 2})})));
} catch (std::exception& e) {
    std::cerr << boost::format("%1%: %2%\n") % typeid(e).name() % e.what();
    return EXIT_FAILURE;
}

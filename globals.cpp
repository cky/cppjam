#include "c++jam.h"

#include <boost/math/constants/constants.hpp>
#include "globals.h"
#include "utils.h"

namespace {
    std::array<cppjam::datum, 26> globals{
            cppjam::int_t(10),                              // A
            cppjam::int_t(11),
            cppjam::int_t(12),
            cppjam::int_t(13),
            cppjam::int_t(14),
            cppjam::int_t(15),                              // F
            cppjam::int_t(16),
            cppjam::int_t(17),
            cppjam::int_t(18),
            cppjam::int_t(19),
            cppjam::int_t(20),                              // K
            cppjam::array_t(),
            cppjam::array_t(),
            cppjam::utils::string_to_datum(U"\n"),
            cppjam::array_t(),
            boost::math::constants::pi<cppjam::float_t>(),  // P
            cppjam::array_t(),
            cppjam::array_t(),
            cppjam::utils::string_to_datum(U" "),
            cppjam::int_t(0),
            cppjam::int_t(0),                               // U
            cppjam::int_t(0),
            cppjam::int_t(-1),
            cppjam::int_t(1),
            cppjam::int_t(2),
            cppjam::int_t(3)                                // Z
    };
}

namespace cppjam {
    datum& global(char name) {
        return globals.at(static_cast<size_t>(name - 'A'));
    }
}

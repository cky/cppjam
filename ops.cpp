#include "c++jam.h"

#include <algorithm>
#include <functional>
#include <iterator>
#include <limits>
#include <numeric>
#include <stdexcept>
#include <type_traits>
#include <unordered_map>
#include <boost/range/irange.hpp>
#include "mixins.h"
#include "ops.h"
#include "utils.h"

namespace {
    template<typename T>
    struct is_numeric : std::false_type {};

    template<>
    struct is_numeric<cppjam::int_t> : std::true_type {};

    template<>
    struct is_numeric<cppjam::float_t> : std::true_type {};

    template<>
    struct is_numeric<cppjam::char_t> : std::true_type {};

    template<typename T>
    struct datum_equal_partial_visitor : boost::static_visitor<bool> {
        T const& lhs_;

        explicit datum_equal_partial_visitor(T const& lhs) : lhs_(lhs) {}

        template<typename U>
        bool operator()(U const&) const {return false;}

        bool operator()(T const& rhs) const {return lhs_ == rhs;}
    };

    struct datum_equal_visitor : boost::static_visitor<bool> {
        cppjam::datum const& rhs_;

        explicit datum_equal_visitor(cppjam::datum const& rhs) : rhs_(rhs) {}

        template<typename T>
        bool operator()(T const& lhs) const {
            return boost::apply_visitor(datum_equal_partial_visitor<T>(lhs), rhs_);
        }
    };

    struct datum_equal {
        bool operator()(cppjam::datum const& lhs, cppjam::datum const& rhs) const {
            return boost::apply_visitor(datum_equal_visitor(rhs), lhs);
        }
    };

    template<>
    struct datum_equal_partial_visitor<cppjam::array_t> : boost::static_visitor<bool> {
        cppjam::array_t const& lhs_;

        explicit datum_equal_partial_visitor(cppjam::array_t const& lhs) : lhs_(lhs) {}

        template<typename U>
        bool operator()(U const&) const {return false;}

        bool operator()(cppjam::array_t const& rhs) const {
            return std::equal(std::begin(lhs_), std::end(lhs_), std::begin(rhs), std::end(rhs), datum_equal());
        }
    };

    struct datum_lessthan_visitor : boost::static_visitor<bool> {
        template<typename T, typename U>
        bool operator()(T const& lhs, U const& rhs) const {
            cppjam::utils::throw_invalid_comparison(lhs, rhs);
        }

        template<typename T, typename = std::enable_if_t<is_numeric<T>::value>>
        bool operator()(T const& lhs, T const& rhs) const {return lhs < rhs;}

        bool operator()(cppjam::int_t const& lhs, cppjam::float_t const& rhs) const {return (*this)(cppjam::float_t(lhs), rhs);}
        bool operator()(cppjam::int_t const& lhs, cppjam::char_t rhs) const {return (*this)(lhs, cppjam::int_t(rhs));}
        bool operator()(cppjam::float_t const& lhs, cppjam::int_t const& rhs) const {return (*this)(lhs, cppjam::float_t(rhs));}
        bool operator()(cppjam::float_t const& lhs, cppjam::char_t rhs) const {return (*this)(lhs, cppjam::int_t(rhs));}
        bool operator()(cppjam::char_t lhs, cppjam::int_t const& rhs) const {return (*this)(cppjam::int_t(lhs), rhs);}
        bool operator()(cppjam::char_t lhs, cppjam::float_t const& rhs) const {return (*this)(cppjam::int_t(lhs), rhs);}

        bool operator()(cppjam::array_t const&, cppjam::array_t const&) const;
    };

    struct datum_lessthan {
        using schwartz = std::pair<cppjam::datum, cppjam::datum>;

        bool operator()(cppjam::datum const& lhs, cppjam::datum const& rhs) const {
            return boost::apply_visitor(datum_lessthan_visitor(), lhs, rhs);
        }

        bool operator()(schwartz const& lhs, schwartz const& rhs) const {
            return boost::apply_visitor(datum_lessthan_visitor(), lhs.second, rhs.second);
        }
    };

    bool datum_lessthan_visitor::operator()(cppjam::array_t const& lhs, cppjam::array_t const& rhs) const {
        return std::lexicographical_compare(std::begin(lhs), std::end(lhs), std::begin(rhs), std::end(rhs), datum_lessthan());
    }

    template<typename T>
    std::pair<char const*, std::shared_ptr<cppjam::stack_op_t>> make_op_pair() {
        return std::make_pair(T::name(), std::make_shared<T>());
    }

    extern std::unordered_map<std::string, std::shared_ptr<cppjam::stack_op_t>> const optab;
}

// Some workarounds for Boost.Multiprecision on Windows, without
// which it would try to instantiate stack_op_t, which is abstract.
namespace boost {
    template<typename T>
    struct is_convertible<cppjam::stack_op_t, T> : std::false_type {};

    namespace multiprecision {
        template<>
        struct number_category<cppjam::stack_op_t> : std::integral_constant<number_category_type, number_kind_unknown> {};

        namespace detail {
            template<typename T>
            struct is_explicitly_convertible<cppjam::stack_op_t, T> : std::false_type {};
        }
    }
}

namespace cppjam {
    stack_op_t const& lookup_op(std::string const& name) {
        return *optab.at(name);
    }

    namespace ops {
        using cppjam::stack_t;

        // See http://sourceforge.net/p/cjam/wiki/Basic%20operators/ for
        // operator documentation.

        struct not_ : virtual stack_op_t, mixins::mixin_base<not_> {
            static constexpr char const* name() {return "!";}

            stack_t operator()(stack_t const& st) const override {
                return stack_t(utils::bool_to_datum(!utils::is_truthy(st.car())), st.cdr());
            }
        };

        struct hash : virtual stack_op_t, mixins::binary_mixin<hash> {
            using mixins::binary_mixin<hash>::dispatch;

            static constexpr char const* name() {return "#";}

            static datum dispatch(int_t const& lhs, int_t const& rhs) {
                if (rhs >= std::numeric_limits<unsigned>::min()
                        && rhs <= std::numeric_limits<unsigned>::max()) {
                    return pow(lhs, static_cast<unsigned>(rhs));
                }
                return dispatch(float_t(lhs), float_t(rhs));
            }

            static datum dispatch(float_t const& lhs, float_t const& rhs) {return pow(lhs, rhs);}

            template<typename T, typename = std::enable_if_t<is_numeric<T>::value>>
            static datum dispatch(array_t const& lhs, T const& rhs) {
                datum_equal_partial_visitor<T> visitor(rhs);
                auto iter(std::find_if(std::begin(lhs), std::end(lhs), boost::apply_visitor(visitor)));
                return int_t(iter != std::end(lhs) ? std::distance(std::begin(lhs), iter) : -1);
            }

            template<typename T, typename = std::enable_if_t<is_numeric<T>::value>>
            static datum dispatch(T const& lhs, array_t const& rhs) {return dispatch(rhs, lhs);}

            static datum dispatch(array_t const& lhs, array_t const& rhs) {
                auto iter(std::search(std::begin(lhs), std::end(lhs), std::begin(rhs), std::end(rhs), datum_equal()));
                return int_t(iter != std::end(lhs) ? std::distance(std::begin(lhs), iter) : -1);
            }

            static datum dispatch(array_t const& lhs, stack_op_t const& rhs, stack_t& st) {
                auto iter(std::find_if(std::begin(lhs), std::end(lhs),
                        [&](datum const& value) {
                    return utils::is_truthy(utils::eval_unary(rhs, value, st));
                }));
                return int_t(iter != std::end(lhs) ? std::distance(std::begin(lhs), iter) : -1);
            }
        };

        struct dollar : virtual stack_op_t, mixins::unary_block_mixin<dollar> {
            using mixins::unary_block_mixin<dollar>::dispatch;

            static constexpr char const* name() {return "$";}

            static datum dispatch(int_t value, stack_t st) {
                if (value >= 0) {
                    while (value--)
                        st = st.cdr();
                } else {
                    auto st2(st);
                    while (value++)
                        st2 = st2.cdr();
                    while (!st2.empty()) {
                        st = st.cdr();
                        st2 = st2.cdr();
                    }
                }
                return st.car();
            }

            static datum dispatch(float_t const& value, stack_t& st) {
                return dispatch(trunc(value).convert_to<int_t>(), st);
            }

            static datum dispatch(char_t value, stack_t& st) {
                return dispatch(int_t(value), st);
            }

            static datum dispatch(array_t arr) {
                std::stable_sort(std::begin(arr), std::end(arr), datum_lessthan());
                return arr;
            }

            static datum dispatch(array_t arr, stack_op_t const& block, stack_t& st) {
                std::vector<datum_lessthan::schwartz> schwartzes(arr.size());
                std::transform(std::begin(arr), std::end(arr), std::begin(schwartzes),
                        [&](datum const& value) {
                    return std::make_pair(value, utils::eval_unary(block, value, st));
                });
                std::stable_sort(std::begin(schwartzes), std::end(schwartzes), datum_lessthan());
                std::transform(std::begin(schwartzes), std::end(schwartzes), std::begin(arr),
                        [](datum_lessthan::schwartz const& schwartz) {return schwartz.first;});
                return arr;
            }
        };

        struct comma : virtual stack_op_t, mixins::unary_block_mixin<comma> {
            using mixins::unary_block_mixin<comma>::dispatch;

            static constexpr char const* name() {return ",";}

            static datum dispatch(int_t const& value) {
                if (value < std::numeric_limits<size_t>::min()
                        || value > std::numeric_limits<size_t>::max())
                    utils::throw_out_of_range(value);
                array_t result(static_cast<size_t>(value));
                std::iota(std::begin(result), std::end(result), int_t(0));
                return result;
            }

            static datum dispatch(float_t const& value) {
                return dispatch(trunc(value).convert_to<int_t>());
            }

            static datum dispatch(char_t value) {
                array_t result(static_cast<size_t>(value));
                auto range(boost::irange(U'\0', static_cast<char32_t>(value)));
                std::transform(std::begin(range), std::end(range), std::begin(result), utils::char_to_datum);
                return result;
            }

            static datum dispatch(array_t const& arr) {
                return int_t(arr.size());
            }

            static datum dispatch(array_t const& arr, stack_op_t const& block, stack_t& st) {
                return filter(arr, block, st);
            }

            static datum dispatch(int_t const& value, stack_op_t const& block, stack_t& st) {
                return filter(boost::irange(int_t(0), value), block, st);
            }

            static datum dispatch(float_t const& value, stack_op_t const& block, stack_t& st) {
                return dispatch(trunc(value).convert_to<int_t>(), block, st);
            }

            static datum dispatch(char_t value, stack_op_t const& block, stack_t& st) {
                return dispatch(int_t(value), block, st);
            }

        private:
            template<typename T>
            static array_t filter(T const& range, stack_op_t const& block, stack_t& st) {
                array_t result;
                std::copy_if(std::begin(range), std::end(range), std::back_inserter(result),
                        [&](datum const& value) {return utils::is_truthy(utils::eval_unary(block, value, st));});
                return result;
            }
        };

        template<typename Op>
        struct minus_base : mixins::binary_mixin<Op> {
            using mixins::binary_mixin<Op>::dispatch;

            static datum dispatch(int_t const& lhs, int_t const& rhs) {return lhs - rhs;}
            static datum dispatch(float_t const& lhs, float_t const& rhs) {return lhs - rhs;}
            static datum dispatch(char_t lhs, char_t rhs) {return int_t(lhs) - int_t(rhs);}
            static datum dispatch(char_t lhs, int_t const& rhs) {return (int_t(lhs) - rhs).convert_to<char_t>();}

            template<typename U, typename = std::enable_if_t<is_numeric<U>::value>>
            static datum dispatch(array_t const& arr, U const& value) {
                array_t result;
                datum_equal_partial_visitor<U> visitor(value);
                std::remove_copy_if(std::begin(arr), std::end(arr), std::back_inserter(result), boost::apply_visitor(visitor));
                return result;
            }

            template<typename T, typename = std::enable_if_t<is_numeric<T>::value>>
            static datum dispatch(T const& value, array_t const& arr) {
                datum_equal_partial_visitor<T> visitor(value);
                return std::any_of(std::begin(arr), std::end(arr), boost::apply_visitor(visitor))
                        ? array_t() : array_t{value};
            }

            static datum dispatch(array_t const& lhs, array_t const& rhs) {
                // O(n^2) :-( but that's no worse than reference impl.
                array_t result;
                datum_equal equal;
                std::remove_copy_if(std::begin(lhs), std::end(lhs), std::back_inserter(result),
                        [&](datum const& lhsval) {
                    return std::any_of(std::begin(rhs), std::end(rhs), [&](datum const& rhsval) {
                        return equal(lhsval, rhsval);
                    });
                });
                return result;
            }
        };

        struct minus1 : virtual stack_op_t, minus_base<minus1> {
            static constexpr char const* name() {return "-";}
        };

        struct minus2 : virtual stack_op_t, minus_base<minus2> {
            static constexpr char const* name() {return "m";}
        };

        struct pop : virtual stack_op_t, mixins::mixin_base<pop> {
            static constexpr char const* name() {return ";";}

            stack_t operator()(stack_t const& st) const override {
                return st.empty() ? st : st.cdr();
            }
        };

        struct rotate3 : virtual stack_op_t, mixins::mixin_base<rotate3> {
            static constexpr char const* name() {return "@";}

            stack_t operator()(stack_t const& st) const override {
                return stack_t(st.cdr().cdr().car(), stack_t(st.car(),
                        stack_t(st.cdr().car(), st.cdr().cdr().cdr())));
            }
        };

        struct swap : virtual stack_op_t, mixins::mixin_base<swap> {
            static constexpr char const* name() {return "\\";}

            stack_t operator()(stack_t const& st) const override {
                return stack_t(st.cdr().car(), stack_t(st.car(), st.cdr().cdr()));
            }
        };

        struct dup : virtual stack_op_t, mixins::mixin_base<dup> {
            static constexpr char const* name() {return "_";}

            stack_t operator()(stack_t const& st) const override {
                return stack_t(st.car(), st);
            }
        };

        struct array : virtual stack_op_t, mixins::mixin_base<array> {
            static constexpr char const* name() {return "a";}

            stack_t operator()(stack_t const& st) const override {
                return stack_t(array_t{st.car()}, st.cdr());
            }
        };
    }
}

namespace {
    std::unordered_map<std::string, std::shared_ptr<cppjam::stack_op_t>> const optab{
            make_op_pair<cppjam::ops::not_>(),
            make_op_pair<cppjam::ops::hash>(),
            make_op_pair<cppjam::ops::dollar>(),
            make_op_pair<cppjam::ops::comma>(),
            make_op_pair<cppjam::ops::minus1>(),
            make_op_pair<cppjam::ops::minus2>(),
            make_op_pair<cppjam::ops::pop>(),
            make_op_pair<cppjam::ops::rotate3>(),
            make_op_pair<cppjam::ops::swap>(),
            make_op_pair<cppjam::ops::dup>(),
            make_op_pair<cppjam::ops::array>(),
    };
}

#pragma once

#include "c++jam.h"
#include <algorithm>
#include <iterator>
#include <string>
#include <boost/format/format_fwd.hpp>
#include <boost/optional.hpp>

namespace cppjam {
    namespace utils {
        using cppjam::stack_t;

        inline datum char_to_datum(char32_t c) {return static_cast<char_t>(c);}

        template<size_t N>
        datum string_to_datum(char32_t const(& str)[N]) {
            array_t result(N - 1);
            std::transform(str, str + N - 1, std::begin(result), char_to_datum);
            return result;
        }

        inline datum bool_to_datum(bool value) {
            return int_t(value);
        }

        inline datum eval_unary(stack_op_t const& block, datum const& arg, stack_t& st) {
            auto st2(block(stack_t(arg, st)));
            st = st2.cdr();
            return st2.car();
        }

        bool is_truthy(datum const&);

        std::string repr(datum const&);
        std::string repr(stack_t const&);

        void throw_invalid_code_point [[noreturn]](char32_t);
        void throw_invalid_comparison [[noreturn]](datum const&, datum const&);
        void throw_invalid_operands [[noreturn]](char const*);
        void throw_invalid_truthiness [[noreturn]]();
        void throw_out_of_range [[noreturn]](int_t const&);
        void throw_stack_underflow [[noreturn]]();

        boost::optional<std::u32string> array_to_string(array_t const&);
        std::string u32string_to_utf8(std::u32string const&);
        std::string char_to_utf8(char_t);
    }
}

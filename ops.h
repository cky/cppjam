#pragma once

#include "c++jam.h"

namespace cppjam {
    stack_op_t const& lookup_op(std::string const&);
}

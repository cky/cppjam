#pragma once

#include <memory>
#include <vector>
#include <boost/multiprecision/cpp_int.hpp>
#if defined(USE_FLOAT128)
#  include <boost/multiprecision/float128.hpp>
#else
#  include <boost/multiprecision/cpp_bin_float.hpp>
#endif
#include <boost/variant.hpp>

namespace cppjam {
    // Redeclare throw_stack_underflow here so this header doesn't have
    // to #include utils.h.
    namespace utils {
        void throw_stack_underflow [[noreturn]]();
    }

    template<typename T>
    class cons {
    public:
        using entry_type = std::pair<T, cons<T>> const;

    private:
        std::shared_ptr<entry_type> value_;

        void ensure_non_empty() const {
            if (empty())
                utils::throw_stack_underflow();
        }

    public:
        cons() = default;

        template<typename U, typename V>
        cons(U&& car, V&& cdr) : value_(std::make_shared<entry_type>(
                std::forward<U>(car), std::forward<V>(cdr))) {}

        bool empty() const {
            return !value_;
        }

        T const& car() const {
            ensure_non_empty();
            return value_->first;
        }

        cons<T> const& cdr() const {
            ensure_non_empty();
            return value_->second;
        }
    };

    using int_t = boost::multiprecision::cpp_int;
#if defined(USE_FLOAT128)
    using float_t = boost::multiprecision::float128;
#else
    using float_t = boost::multiprecision::cpp_bin_float_quad;
#endif

    using char_t = char32_t;
    struct stack_op_t;

#if !defined(BOOST_VARIANT_NO_FULL_RECURSIVE_VARIANT_SUPPORT)
    using datum = boost::make_recursive_variant<int_t, float_t, char_t,
            std::vector<boost::recursive_variant_>,
            std::shared_ptr<stack_op_t>>::type;
    using array_t = std::vector<datum>;
#else
    struct array_t;
    using datum = boost::variant<int_t, float_t, char_t,
            boost::recursive_wrapper<array_t>,
            std::shared_ptr<stack_op_t>>;
    struct array_t : std::vector<datum> {
        using base = std::vector<datum>;
        using base::base;
    };
#endif

    using stack_t = cons<datum>;
    using stack_entry = stack_t::entry_type;

    struct stack_op_t {
        virtual char const* repr() const = 0;
        virtual stack_t operator()(stack_t const&) const = 0;
        virtual ~stack_op_t() = default;
    };
}

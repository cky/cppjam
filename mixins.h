#pragma once

#include "c++jam.h"
#include <boost/format.hpp>
#include <boost/variant.hpp>
#include "utils.h"

namespace cppjam {
    namespace mixins {
        using cppjam::stack_t;

        namespace impl {
            struct type_mapper {
                template<typename T>
                static T const& map(T const& value) {return value;}

                static stack_op_t const& map(std::shared_ptr<stack_op_t> const& value) {
                    return *value;
                }
            };

            template<typename Op>
            struct unary_visitor : boost::static_visitor<datum> {
                stack_t& st_;

                explicit unary_visitor(stack_t& st) : st_(st) {}

                template<typename T>
                datum operator()(T const& value) const {
                    return Op::dispatch(type_mapper::map(value), st_);
                }
            };

            template<typename Op>
            struct unary_block_visitor : boost::static_visitor<datum> {
                stack_op_t const& block_;
                stack_t& st_;

                unary_block_visitor(stack_op_t const& block, stack_t& st) : block_(block), st_(st) {}

                template<typename T>
                datum operator()(T const& value) const {
                    return Op::dispatch(type_mapper::map(value), block_, st_);
                }
            };

            template<typename Op>
            struct binary_visitor : boost::static_visitor<datum> {
                stack_t& st_;

                explicit binary_visitor(stack_t& st) : st_(st) {}

                template<typename T, typename U>
                datum operator()(T const& lhs, U const& rhs) const {
                    return Op::dispatch(type_mapper::map(lhs), type_mapper::map(rhs), st_);
                }
            };

            template<typename Op>
            static void throw_not_implemented [[noreturn]]() {
                utils::throw_invalid_operands(Op::name());
            }
        }

        template<typename Op>
        struct mixin_base : virtual stack_op_t {
            char const* repr() const override {return Op::name();}
        };

        template<typename Op>
        struct unary_mixin : mixin_base<Op> {
            stack_t operator()(stack_t const& st) const override {
                auto st2(st.cdr());
                auto result(boost::apply_visitor(impl::unary_visitor<Op>(st2), st.car()));
                return stack_t(result, st2);
            }

            template<typename T>
            static datum dispatch(T const&) {impl::throw_not_implemented<Op>();}

            template<typename T>
            static datum dispatch(T const& value, stack_t&) {return Op::dispatch(value);}

            static datum dispatch(int_t const& value) {return Op::dispatch(float_t(value));}
            static datum dispatch(char_t value) {return Op::dispatch(int_t(value));}
        };

        template<typename Op>
        struct unary_block_mixin : unary_mixin<Op> {
            using unary_mixin<Op>::dispatch;

            static datum dispatch(stack_op_t const& block, stack_t& st) {
                auto st2(st.cdr());
                auto result(boost::apply_visitor(impl::unary_block_visitor<Op>(block, st2), st.car()));
                st = st2;
                return result;
            }

            template<typename T>
            static datum dispatch(T const&, stack_op_t const&, stack_t&) {impl::throw_not_implemented<Op>();}
        };

        template<typename Op>
        struct binary_mixin : mixin_base<Op> {
            stack_t operator()(stack_t const& st) const override {
                auto st2(st.cdr().cdr());
                auto result(boost::apply_visitor(impl::binary_visitor<Op>(st2), st.cdr().car(), st.car()));
                return stack_t(result, st2);
            }

            template<typename T, typename U>
            static datum dispatch(T const&, U const&) {impl::throw_not_implemented<Op>();}

            template<typename T, typename U>
            static datum dispatch(T const& lhs, U const& rhs, stack_t&) {return Op::dispatch(lhs, rhs);}

            static datum dispatch(int_t const& lhs, int_t const& rhs) {return Op::dispatch(float_t(lhs), float_t(rhs));}
            static datum dispatch(int_t const& lhs, float_t const& rhs) {return Op::dispatch(float_t(lhs), rhs);}
            static datum dispatch(int_t const& lhs, char_t rhs) {return Op::dispatch(lhs, int_t(rhs));}

            static datum dispatch(float_t const& lhs, int_t const& rhs) {return Op::dispatch(lhs, float_t(rhs));}
            static datum dispatch(float_t const& lhs, char_t rhs) {return Op::dispatch(lhs, int_t(rhs));}

            static datum dispatch(char_t lhs, int_t const& rhs) {return Op::dispatch(int_t(lhs), rhs);}
            static datum dispatch(char_t lhs, float_t const& rhs) {return Op::dispatch(int_t(lhs), rhs);}
            static datum dispatch(char_t lhs, char_t rhs) {return Op::dispatch(int_t(lhs), int_t(rhs));}

            static datum dispatch(stack_op_t const& lhs, int_t const& rhs, stack_t& st) {return Op::dispatch(rhs, lhs, st);}
            static datum dispatch(stack_op_t const& lhs, float_t const& rhs, stack_t& st) {return Op::dispatch(rhs, lhs, st);}
            static datum dispatch(stack_op_t const& lhs, char_t rhs, stack_t& st) {return Op::dispatch(rhs, lhs, st);}
            static datum dispatch(stack_op_t const& lhs, array_t const& rhs, stack_t& st) {return Op::dispatch(rhs, lhs, st);}
        };
    }
}

#include "c++jam.h"

#include <sstream>
#include <stdexcept>
#include <boost/format.hpp>
#include "utils.h"

namespace {
    struct truthy_visitor : boost::static_visitor<bool> {
        bool operator()(cppjam::int_t const& value) const {
            return !value.is_zero();
        }

        bool operator()(cppjam::float_t const& value) const {
            return !value.is_zero();
        }

        bool operator()(cppjam::char_t value) const {
            return value != 0;
        }

        bool operator()(cppjam::array_t const& value) const {
            return !value.empty();
        }

        bool operator()(std::shared_ptr<cppjam::stack_op_t> const&) const {
            cppjam::utils::throw_invalid_truthiness();
        }
    };

    struct repr_visitor : boost::static_visitor<> {
        std::ostream& out_;

        repr_visitor(std::ostream& out) : out_(out) {}

        void operator()(cppjam::int_t const& value) const {
            out_ << value;
        }

        void operator()(cppjam::float_t const& value) const {
            out_ << value;
        }

        void operator()(cppjam::char_t value) const {
            out_ << '\'' << cppjam::utils::char_to_utf8(value);
        }

        void operator()(cppjam::array_t const& arr) const {
            if (auto str = cppjam::utils::array_to_string(arr)) {
                out_ << '"' << cppjam::utils::u32string_to_utf8(*str) << '"';
            } else {
                out_ << '[';
                auto first(true);
                for (auto const& value : arr) {
                    if (!first)
                        out_ << ' ';
                    first = false;
                    boost::apply_visitor(*this, value);
                }
                out_ << ']';
            }
        }

        void operator()(std::shared_ptr<cppjam::stack_op_t> const& block) const {
            out_ << block->repr();
        }
    };

    struct invalid_code_point : std::runtime_error {
        invalid_code_point(char32_t value) : std::runtime_error(
                boost::str(boost::format("Invalid code point %#x") % value)) {}
    };

    struct invalid_comparison : std::runtime_error {
        invalid_comparison(cppjam::datum const& lhs, cppjam::datum const& rhs)
                : std::runtime_error(boost::str(boost::format(
                "Cannot compare between %1% and %2%") % cppjam::utils::repr(lhs)
                % cppjam::utils::repr(rhs))) {}
    };

    struct invalid_operands : std::runtime_error {
        invalid_operands(char const* name) : std::runtime_error(boost::str(
                boost::format("The %1% operator is not defined for the given operands")
                % name)) {}
    };

    struct invalid_truthiness : std::runtime_error {
        invalid_truthiness() : std::runtime_error("Can't test blocks for truthiness") {}
    };

    struct stack_underflow : std::runtime_error {
        stack_underflow() : std::runtime_error("Stack underflow") {}
    };
}

namespace cppjam {
    namespace utils {
        bool is_truthy(datum const& value) {
            return boost::apply_visitor(truthy_visitor(), value);
        }

        void repr(std::ostream& out, datum const& value) {
            boost::apply_visitor(repr_visitor(out), value);
        }

        std::string repr(datum const& value) {
            std::ostringstream out;
            repr(out, value);
            return out.str();
        }

        void repr(std::ostream& out, stack_t const& st) {
            if (!st.empty()) {
                repr(out, st.cdr());
                if (!st.cdr().empty())
                    out << ' ';
                repr(out, st.car());
            }
        }

        std::string repr(stack_t const& st) {
            std::ostringstream out;
            repr(out, st);
            return out.str();
        }

        void throw_invalid_code_point [[noreturn]](char32_t value) {
            throw invalid_code_point(value);
        }

        void throw_invalid_comparison [[noreturn]](datum const& lhs, datum const& rhs) {
            throw invalid_comparison(lhs, rhs);
        }

        void throw_invalid_operands [[noreturn]](char const* name) {
            throw invalid_operands(name);
        }

        void throw_invalid_truthiness [[noreturn]]() {
            throw invalid_truthiness();
        }

        void throw_out_of_range [[noreturn]](int_t const& value) {
            throw std::range_error(boost::str(boost::format("Value out of range: %1%") % value));
        }

        void throw_stack_underflow [[noreturn]]() {
            throw stack_underflow();
        }

        boost::optional<std::u32string> array_to_string(array_t const& arr) {
            std::u32string str;
            for (auto const& x : arr) {
                if (auto c = boost::get<char_t>(&x)) {
                    if (*c == U'"' || *c == U'\\')
                        str.push_back(U'\\');
                    str.push_back(*c);
                } else {
                    return boost::none;
                }
            }
            return str;
        }

        std::string u32string_to_utf8(std::u32string const& str) {
            std::string result;
            auto push([&](auto c) {result.push_back(static_cast<char>(c));});
            // codecvt_utf8<char32_t> doesn't work in VS2015, so use
            // manual UTF-8 conversion for now.
            for (auto c : str) {
                if ((c >= 0xD800 && c < 0xE000) || c >= 0x110000)
                    throw_invalid_code_point(c);
                if (c < 0x80) {
                    push(c);
                } else if (c < 0x800) {
                    push(0xC0 | (c >> 6));
                    push(0x80 | (c & 0x3F));
                } else if (c < 0x10000) {
                    push(0xE0 | (c >> 12));
                    push(0x80 | ((c >> 6) & 0x3F));
                    push(0x80 | (c & 0x3F));
                } else {
                    push(0xF0 | (c >> 18));
                    push(0x80 | ((c >> 12) & 0x3F));
                    push(0x80 | ((c >> 6) & 0x3F));
                    push(0x80 | (c & 0x3F));
                }
            }
            return result;
        }

        std::string char_to_utf8(char_t value) {
            return u32string_to_utf8(std::u32string(1, static_cast<char32_t>(value)));
        }
    }
}
